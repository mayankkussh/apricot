package com.iitbbhaskar.apricot.api

object UrlContants {
    const val GENERATE_OTP = "api/v1/login/generate-otp/"
    const val LOGIN = "api/v1/login/verify-otp/"
}