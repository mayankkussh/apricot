package com.iitbbhaskar.apricot.api.generateotp

import com.squareup.moshi.Json

data class GenerateOtpRequest(

    @Json(name = "device_id")
    val deviceId: String? = null,

    @Json(name = "phone")
    val phone: Long? = null,

    @Json(name = "app_id")
    val appId: String? = null
)