package com.iitbbhaskar.apricot.api.login

import com.squareup.moshi.Json

data class LoginRequest(

    @Json(name = "device_id")
    val deviceId: String? = null,

    @Json(name = "phone")
    val phone: Long? = null,

    @Json(name = "otp")
    val otp: String? = null,

    @Json(name = "app_id")
    val appId: String? = null
)