package com.iitbbhaskar.apricot.api.login.response

import com.squareup.moshi.Json

data class LoginResponse(

	@Json(name="value")
	val value: Value
)