package com.iitbbhaskar.apricot.api.login.response

import com.squareup.moshi.Json

data class Value(

	@Json(name="user_id")
	val userId: String,

	@Json(name="pseudo_id_list")
	val pseudoIdList: List<String>,

	@Json(name="token")
	val token: String
)