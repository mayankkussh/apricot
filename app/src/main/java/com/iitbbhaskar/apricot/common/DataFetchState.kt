package com.iitbbhaskar.apricot.common

import com.iitbbhaskar.apricot.network.StandardizedError


/**
 * possible states of any data state machine
 * Created by mayankkush on 16/01/18.
 */
sealed class DataFetchState {
  class Loading : DataFetchState()
  class Success : DataFetchState()
  class Error(val error: StandardizedError) : DataFetchState()

}