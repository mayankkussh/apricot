package com.iitbbhaskar.apricot.common

import com.iitbbhaskar.apricot.network.StandardizedError

data class Validation(val status: Boolean, val error: StandardizedError?) {
}