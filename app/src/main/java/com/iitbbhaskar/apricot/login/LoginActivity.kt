package com.iitbbhaskar.apricot.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.iitbbhaskar.apricot.MainActivity
import com.iitbbhaskar.apricot.R
import com.iitbbhaskar.apricot.login.otp.OtpValidationFragment
import com.iitbbhaskar.apricot.login.phone.PhoneFragment

class LoginActivity : AppCompatActivity(), LoginFragmentActions {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        //fragmentTransaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right);
        // Replace the contents of the container with the new fragment
        fragmentTransaction.add(R.id.fragment, PhoneFragment.newInstance()).setTransition(
            androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN
        )
        // or ft.add(R.id.your_placeholder, new FooFragment());
        // Complete the changes added above
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun onOtpSend(phoneNumber: String) {
        // change fragment
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        //fragmentTransaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right);
        // Replace the contents of the container with the new fragment
        fragmentTransaction.replace(R.id.fragment, OtpValidationFragment.newInstance(phoneNumber))
            .addToBackStack("")
            .setTransition(
                androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN
            )
        // or ft.add(R.id.your_placeholder, new FooFragment());
        // Complete the changes added above
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun onLoginSuccess() {
        finish()
        Intent(this, MainActivity::class.java).also {
            startActivity(it)
        }
    }
}
