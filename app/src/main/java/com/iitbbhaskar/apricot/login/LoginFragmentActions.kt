package com.iitbbhaskar.apricot.login

interface LoginFragmentActions {
    fun onOtpSend(phoneNumber: String)
    fun onLoginSuccess()
}