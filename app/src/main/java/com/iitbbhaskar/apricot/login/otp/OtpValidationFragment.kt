package com.iitbbhaskar.apricot.login.otp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.iitbbhaskar.apricot.R
import com.iitbbhaskar.apricot.common.DataFetchState
import com.iitbbhaskar.apricot.login.LoginFragmentActions
import com.iitbbhaskar.apricot.network.StandardizedError
import kotlinx.android.synthetic.main.fragment_login_otp.*

class OtpValidationFragment : Fragment() {

    private val viewModel by viewModels<OtpValidationViewModel>()


    companion object {
        private const val ARGS_PHONE_NUMBER = "phone_number"
        fun newInstance(phoneNumber: String): OtpValidationFragment {
            return OtpValidationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARGS_PHONE_NUMBER, phoneNumber)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getString(ARGS_PHONE_NUMBER)?.run {
            viewModel.initialize(this)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login_otp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.phoneNumberText.observe(viewLifecycleOwner, Observer {
            phone_number.text = it
        })

        viewModel.loginStatus.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DataFetchState.Loading -> showLogginIn()
                is DataFetchState.Success -> onLoginSuccess()
                is DataFetchState.Error -> onLoginError(it.error)
            }
        })

        viewModel.resendStateMachine.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DataFetchState.Loading -> showResendingOtp()
                is DataFetchState.Success -> onOtpResendSuccess()
                is DataFetchState.Error -> onOtpResendError(it.error)
            }
        })

        login.setOnClickListener {
            viewModel.onLoginClick(otp.text.toString())
        }

        resend.setOnClickListener {
            viewModel.onResendClick()
        }


    }

    private fun onOtpResendError(error: StandardizedError) {
        resend_progress_bar.visibility = View.GONE
        Toast.makeText(context, error.displayError, Toast.LENGTH_LONG).show()
        resend.visibility = View.VISIBLE
    }

    private fun onOtpResendSuccess() {
        resend_progress_bar.visibility = View.GONE
        Toast.makeText(context, "OTP Sent", Toast.LENGTH_LONG).show()
        resend.visibility = View.VISIBLE
    }

    private fun showResendingOtp() {
        resend_progress_bar.visibility = View.VISIBLE
        resend.visibility = View.GONE
    }

    private fun onLoginError(error: StandardizedError) {
        login.visibility = View.VISIBLE
        login_progress_bar.visibility = View.GONE
        Toast.makeText(context, error.displayError, Toast.LENGTH_LONG).show()
    }

    private fun onLoginSuccess() {
        login.visibility = View.VISIBLE
        login_progress_bar.visibility = View.GONE
        if (activity is LoginFragmentActions) {
            (activity as LoginFragmentActions).onLoginSuccess()
        }
        Toast.makeText(context, "Login Successful", Toast.LENGTH_LONG).show()
    }

    private fun showLogginIn() {
        login_progress_bar.visibility = View.VISIBLE
        login.visibility = View.GONE
    }
}