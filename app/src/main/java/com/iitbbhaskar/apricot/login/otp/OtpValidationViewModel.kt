package com.iitbbhaskar.apricot.login.otp

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.iitbbhaskar.apricot.api.generateotp.GenerateOtpRequest
import com.iitbbhaskar.apricot.api.login.LoginRequest
import com.iitbbhaskar.apricot.api.login.response.LoginResponse
import com.iitbbhaskar.apricot.common.DataFetchState
import com.iitbbhaskar.apricot.common.Validation
import com.iitbbhaskar.apricot.network.ApiResponse
import com.iitbbhaskar.apricot.network.StandardizedError
import com.iitbbhaskar.apricot.network.client.apiV1networkRequest
import com.iitbbhaskar.apricot.user.User

class OtpValidationViewModel(application: Application) : AndroidViewModel(application) {

    val phoneNumberText = MutableLiveData<String>()

    val resendStateMachine = MutableLiveData<DataFetchState>()

    val loginStatus = MutableLiveData<DataFetchState>()

    lateinit var phoneNumber: String

    fun initialize(phoneNumber: String) {
        this.phoneNumber = phoneNumber
        phoneNumberText.postValue("OTP has been sent to : $phoneNumber")
    }

    fun onLoginClick(otp: String) {
        val validation = isFormValid(otp)
        if (validation.status) {
            loginStatus.postValue(DataFetchState.Loading())
            apiV1networkRequest(object : ApiResponse<LoginResponse> {
                override fun onSuccess(response: LoginResponse) {
                    saveUser(getUser(response))
                    loginStatus.postValue(DataFetchState.Success())
                }

                override fun onError(error: StandardizedError) {
                    loginStatus.postValue(DataFetchState.Error(error))
                }

            }) {
                login(LoginRequest("123", phoneNumber?.toLong(), otp, "200001"))
            }
        } else {
            validation.error?.run {
                loginStatus.postValue(DataFetchState.Error(this))
            }

        }
    }

    private fun getUser(response: LoginResponse): User {
        return response.value.run {
            User(token, userId, pseudoIdList)
        }
    }

    private fun saveUser(user: User) {
        User.saveUser(getApplication(), user)
    }

    private fun isFormValid(otp: String): Validation {
        if (otp.length == 4) {
            return Validation(true, null)
        }
        return Validation(false, StandardizedError(0, "Invalid Otp", "Please enter a valid otp"))
    }

    fun onResendClick() {
        // make the api call
        apiV1networkRequest(object : ApiResponse<Any> {
            override fun onSuccess(response: Any) {
                resendStateMachine.postValue(DataFetchState.Success())
            }

            override fun onError(error: StandardizedError) {
                resendStateMachine.postValue(DataFetchState.Error(error))
            }

        }) {
            generateOtp(GenerateOtpRequest("123", phoneNumber.toLong(), "200001"))
        }
    }
}