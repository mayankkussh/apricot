package com.iitbbhaskar.apricot.login.phone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.iitbbhaskar.apricot.R
import com.iitbbhaskar.apricot.login.LoginFragmentActions
import com.iitbbhaskar.apricot.login.phone.model.PhoneSubmitForm
import kotlinx.android.synthetic.main.fragment_login_phone.*

class PhoneFragment : Fragment() {

    private val viewModel: PhoneViewModel by viewModels()

    companion object {
        fun newInstance(): PhoneFragment {
            return PhoneFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.otpSendStatus.observe(this, Observer {
            if (it.status) {
                if (activity is LoginFragmentActions) {
                    (activity as LoginFragmentActions).onOtpSend(it.number)
                }
            }
        })

        viewModel.validationError.observe(this, Observer {
            phone_number.error = it.displayError

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login_phone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        submit.setOnClickListener {
            viewModel.onPhoneNumberSubmit(getFormData())
        }
    }

    private fun getFormData(): PhoneSubmitForm {
        return PhoneSubmitForm(phone_number.text.toString())
    }

}