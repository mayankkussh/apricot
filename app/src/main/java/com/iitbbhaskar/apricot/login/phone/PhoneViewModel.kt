package com.iitbbhaskar.apricot.login.phone

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.iitbbhaskar.apricot.api.generateotp.GenerateOtpRequest
import com.iitbbhaskar.apricot.common.Validation
import com.iitbbhaskar.apricot.login.phone.model.OtpSend
import com.iitbbhaskar.apricot.login.phone.model.PhoneSubmitForm
import com.iitbbhaskar.apricot.network.ApiResponse
import com.iitbbhaskar.apricot.network.StandardizedError
import com.iitbbhaskar.apricot.network.client.apiV1networkRequest

class PhoneViewModel : ViewModel() {

    val validationError = MutableLiveData<StandardizedError>()
    val otpSendStatus = MutableLiveData<OtpSend>()

    fun onPhoneNumberSubmit(phoneSubmitForm: PhoneSubmitForm) {
        val validation = isFormValid(phoneSubmitForm)
        if (validation.status) {
            submitPhoneNumber(phoneSubmitForm.phone!!)
            return
        }
        validationError.postValue(validation.error)
    }

    private fun submitPhoneNumber(phone: String) {
        // make the api call
        apiV1networkRequest(object : ApiResponse<Any> {
            override fun onSuccess(response: Any) {
                otpSendStatus.postValue(OtpSend(true, phone))
            }

            override fun onError(error: StandardizedError) {
                validationError.postValue(error)
            }

        }) {
            generateOtp(GenerateOtpRequest("123", phone.toLong(), "200001"))
        }
    }

    private fun isFormValid(phoneSubmitForm: PhoneSubmitForm): Validation {
        if (phoneSubmitForm.phone?.length != 10 && phoneSubmitForm.phone?.toLongOrNull() != null) {
            return Validation(
                false,
                StandardizedError(0, "Invalid Phone", "Please enter 10 digit phone")
            )
        }
        return Validation(true, null)
    }

}