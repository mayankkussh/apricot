package com.iitbbhaskar.apricot.network

/**
 * Callback for data response from API, all NAO returns their data through this callback
 * only
 */
interface ApiResponse<in T>{
  fun onSuccess(response: T)
  fun onError(error: StandardizedError)
}