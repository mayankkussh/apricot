package com.iitbbhaskar.apricot.network

import androidx.multidex.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.iitbbhaskar.apricot.network.interceptors.HeaderInterceptor
import com.squareup.moshi.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit.SECONDS


/**
 * retrofit client for all the retrofit requests
 */
object RetrofitClient {

    private const val CONNECT_TIMEOUT = 15L
    private const val READ_TIMEOUT = 15L
    private lateinit var retrofit: Retrofit


    val headerInterceptor = HeaderInterceptor("")

    fun getRetrofitClient(
        baseUrl: String,
        responseProcessor: (requestCode: Int?, request: Request, response: Response) -> Unit
    ):
            Retrofit {
        if (!RetrofitClient::retrofit.isInitialized) {
            //val types = Types.newParameterizedType(Map::class.java, String::class.java, String::class.java)
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
           /* if (BuildConfig.DEBUG)
            else
                loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE*/

            val moshiBuilder = Moshi.Builder().add(KotlinJsonAdapterFactory())
            val type = Types.newParameterizedType(
                Map::class.java, String::class.java,
                Any::class.javaObjectType
            )

            val moshi = moshiBuilder.build()
            val mapAdapter = moshi.adapter<Map<String, String>>(type)

            val client = OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, SECONDS)
                .readTimeout(READ_TIMEOUT, SECONDS)
                .addInterceptor(headerInterceptor)
                .addInterceptor(loggingInterceptor)
                .build()

            retrofit = Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi)).client(client).build()
        }
        return retrofit
    }
}