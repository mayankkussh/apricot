package com.iitbbhaskar.apricot.network

import android.util.Log

/**
 * model to contain standardized error
 */
data class StandardizedError(
    val responseCode: Int, val developerError: String,
    val displayError: String, val icon: Int? = null, val url: String? = ""
) : Throwable(
    developerError
) {


    init {
        val exceptionAttibutes = HashMap<String, Any>()
        exceptionAttibutes.put("response_code", responseCode)
        exceptionAttibutes.put("developer_error", developerError)
        exceptionAttibutes.put("display_error", displayError)
        val msg = "$developerError in $url"
        if (url != null)
            exceptionAttibutes.put("url_data", url)
        Log.d(StandardizedError::class.java.simpleName, msg)
    }

    class HandledException(msg: String) : Exception(msg)
}