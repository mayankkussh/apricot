package com.iitbbhaskar.apricot.network.client

import com.iitbbhaskar.apricot.api.UrlContants
import com.iitbbhaskar.apricot.api.generateotp.GenerateOtpRequest
import com.iitbbhaskar.apricot.api.login.LoginRequest
import com.iitbbhaskar.apricot.api.login.response.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST(UrlContants.GENERATE_OTP)
    fun generateOtp(@Body generateOtpRequest: GenerateOtpRequest): Call<Any>

    @POST(UrlContants.LOGIN)
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>

}