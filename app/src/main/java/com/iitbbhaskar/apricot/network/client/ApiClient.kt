package com.iitbbhaskar.apricot.network.client

import com.iitbbhaskar.apricot.network.RetrofitClient
import okhttp3.Request
import okhttp3.Response

object ApiClient {

    private const val API_BASE_UR = "https://apricot.safe-analytics.in"
    private var apiClient: Api? = null

    fun getApiClient(): Api {
        if (apiClient == null) {
            apiClient = RetrofitClient.getRetrofitClient(API_BASE_UR) { responseCode: Int?, request:
            Request, response: Response ->
                // Do nothing
            }.create(Api::class.java)
        }
        return apiClient!!
    }
}