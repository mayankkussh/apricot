package com.iitbbhaskar.apricot.network.client

import com.iitbbhaskar.apricot.network.ApiResponse
import com.iitbbhaskar.apricot.network.helpers.makeApiCall
import retrofit2.Call

fun <W> apiV1networkRequest(dataResponse: ApiResponse<W>,
                            apiMethod: Api.() -> Call<W>
) {
    val wmgV1Api = ApiClient.getApiClient()
    makeApiCall(dataResponse, wmgV1Api, {
        (this as Api).apiMethod()
    })
}