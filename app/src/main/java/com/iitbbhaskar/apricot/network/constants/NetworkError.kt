package com.iitbbhaskar.apricot.network.constants

/**
 * Error codes when fetching data from network
 */
object NetworkError {
  const val NULL_RESPONSE_CODE = 1
  const val NO_INTERNET_CODE = 2
  const val MALFORMED_JSON_CODE = 4
  const val UNKNOWN_ERROR_CODE = 3
  const val DEEPLINK_CODE = 5

}