package com.iitbbhaskar.apricot.network.helpers

import com.iitbbhaskar.apricot.network.ApiResponse
import com.iitbbhaskar.apricot.network.transformer.NetworkErrorTransformer
import retrofit2.Callback
import retrofit2.Response

fun <T, W> makeApiCall(apiResponse: ApiResponse<W>?, apiClient: T, apiMethod: T.() -> retrofit2.Call<W>) {
  makeApiCall(apiClient, apiResponse) {
    apiMethod()
  }
}

private fun <R, T> makeApiCall(
    apiClient: T,
    apiResponse: ApiResponse<R>?,
    networkFunction: T.() -> retrofit2.Call<R>
) {
  apiClient.networkFunction().enqueue(object : Callback<R> {
    override fun onFailure(call: retrofit2.Call<R>, t: Throwable) {
      apiResponse?.onError(NetworkErrorTransformer.getStandardizedError(t, call))
    }

    override fun onResponse(call: retrofit2.Call<R>, response: Response<R>) {
      val body = response.body()
      if (response.isSuccessful && body != null) {
        apiResponse?.onSuccess(body)
      } else {
        apiResponse?.onError(NetworkErrorTransformer.getStandardizedError(response))
      }
    }
  })
}
