package com.iitbbhaskar.apricot.network.interceptors

import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody


class ErrorInterceptor(val sentinalUrl: String, private val responseProcessor:
(requestCode: Int?, request: Request, response: Response) -> Unit
) :
    Interceptor {
  override fun intercept(chain: Chain?): Response {

    val request = chain!!.request()
    val response = chain.proceed(request)
    val responseBody = response.body()
    val responseBodyString = responseBody?.string()
    try {
      if (!request.url().url().toString().contains(sentinalUrl)) {
        responseProcessor(response?.code(), request, response)
      }
    } catch (e: Exception) {
      // Do nothing
      e.printStackTrace()
    }
    return response.newBuilder().body(
        ResponseBody.create(responseBody?.contentType(), responseBodyString?.toByteArray()!!)
    ).build()
  }
}