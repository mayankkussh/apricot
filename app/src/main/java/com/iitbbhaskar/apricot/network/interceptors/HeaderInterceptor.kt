package com.iitbbhaskar.apricot.network.interceptors

import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response


/**
 * Interceptor to add headers in every retrofit request
 */
class HeaderInterceptor(var token: String) : Interceptor {

  override fun intercept(chain: Chain): Response {
    val request = chain.request().newBuilder()
        .addHeader("source", "2")
        .addHeader("token", token)
        .build()
    return chain.proceed(request)
  }
}