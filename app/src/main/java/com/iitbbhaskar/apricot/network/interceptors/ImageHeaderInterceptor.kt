package com.iitbbhaskar.apricot.network.interceptors

import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Request
import okhttp3.Response


class ImageHeaderInterceptor : Interceptor {

  override fun intercept(chain: Chain?): Response? {
    val request = chain?.request()
    val newRequest: Request?

    newRequest = request?.newBuilder()?.addHeader("Accept", "image/webp")?.addHeader("dpr", "1")?.build()
    return chain?.proceed(newRequest!!)
  }
}