package com.iitbbhaskar.apricot.network.model

import com.squareup.moshi.Json

class ApiError(@Json(name = "errorMessage") val errorMessage: String,
    @Json(name = "errorCode") val errorCode: Int) {
}