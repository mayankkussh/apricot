package com.iitbbhaskar.apricot.network.model

import com.squareup.moshi.Json

data class ApiErrorPhp(

	@Json(name="error")
	val error: Error? = null
)