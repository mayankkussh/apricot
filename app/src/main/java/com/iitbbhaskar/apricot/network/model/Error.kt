package com.iitbbhaskar.apricot.network.model

import com.squareup.moshi.Json

data class Error(

	@Json(name="error_detail")
	val errorDetail: ErrorDetail? = null,

	@Json(name="message")
	val message: String? = null
)