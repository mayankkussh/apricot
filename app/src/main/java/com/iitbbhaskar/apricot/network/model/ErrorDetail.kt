package com.iitbbhaskar.apricot.network.model

import com.squareup.moshi.Json

data class ErrorDetail(

	@Json(name="message")
	val message: String? = null
)