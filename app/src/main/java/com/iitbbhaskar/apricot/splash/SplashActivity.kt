package com.iitbbhaskar.apricot.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.iitbbhaskar.apricot.MainActivity
import com.iitbbhaskar.apricot.R
import com.iitbbhaskar.apricot.login.LoginActivity
import com.iitbbhaskar.apricot.user.User
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        setContentView(R.layout.activity_splash)

        Timer().schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    finish()
                    moveToNextPage()
                }
            }

        }, 3000)

    }


    private fun moveToNextPage() {
        val user = User.getUser(this)
        if (user != null) {
            Intent(this, MainActivity::class.java).also {
                startActivity(it)
            }
        } else {
            Intent(this, LoginActivity::class.java).also {
                startActivity(it)
            }
        }
    }
}
