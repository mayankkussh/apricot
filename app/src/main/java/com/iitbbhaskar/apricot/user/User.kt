package com.iitbbhaskar.apricot.user

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson

data class User(val token: String, val userId: String, val pseudoIds: List<String>) {

    companion object {
        private const val USER = "pref_user"
        fun getUser(context: Context): User? {
            val json = PreferenceManager.getDefaultSharedPreferences(context).getString(USER, null)
            if (json != null)
                return Gson().fromJson(json, User::class.java)
            return null
        }

        fun saveUser(context: Context, user: User) {
            PreferenceManager.getDefaultSharedPreferences(context).edit {
                putString(USER, Gson().toJson(user))
            }
        }

        fun logout(context: Context) {
            PreferenceManager.getDefaultSharedPreferences(context).edit { remove(USER) }
        }
    }
}